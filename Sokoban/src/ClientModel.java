import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
public class ClientModel implements Model {
    private Player[] players;
    Socket socket;
    View view;
    LevelOne levelOne;
    private ObjectOutputStream dir;
    @Override
    public Player[] getPlayers() {
        return players;
    }
    @Override
    public void doStep(Direction dirNowFS, int player) {
        try {
            dir.writeObject(dirNowFS);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void setPlayers(Player[] players) {
        this.players = players;
        view.repaint();
    }
    public ClientModel(View view, Socket socket, LevelOne lv1){
        this.levelOne=lv1;
        this.view = view;
        this.socket = socket;
        players = new Player[2];
        players[0] = new Player(view.getWidth()/(2*DOT_SIZE)+2,view.getHeight()/DOT_SIZE-2);
        players[1] = new Player(view.getWidth()/(2*DOT_SIZE)-2,view.getHeight()/DOT_SIZE-2);
        try {
            dir = new ObjectOutputStream(socket.getOutputStream());
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
