import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
public class ServerModel implements Model {
    private View view;
    public Player[] getPlayers() {
        return players;
    }
    private Player[] players;
    private DataOutputStream out;
    Socket socket;
    LevelOne levelOne;
    public ServerModel(View view, Socket socket, LevelOne lv1){
        this.view=view;
        this.levelOne = lv1;
        players = new Player[2];
        players[0] = new Player(view.getWidth()/(2*DOT_SIZE)-2,view.getHeight()/DOT_SIZE-2);
        players[1] = new Player(view.getWidth()/(2*DOT_SIZE)+2,view.getHeight()/DOT_SIZE-2);
        this.socket = socket;
        try {
            out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    public void checkBorder(){
        if (players[0].x*DOT_SIZE<=0|players[0].y*DOT_SIZE>=view.getHeight()|players[0].y*DOT_SIZE==0){
            Player[] players1 = getPlayers();
            this.players[1]=players1[1];
            players1[0].x = view.getWidth()/(2*DOT_SIZE)-2;
            players1[0].y = view.getHeight()/DOT_SIZE-2;
            setPlayers(players1);
        }
        if (players[1].x*DOT_SIZE>=view.getWidth()|players[1].y*DOT_SIZE>=view.getHeight()|players[1].y*DOT_SIZE==0){
            Player[] players1 = getPlayers();
            this.players[0]=players1[0];
            players1[1].x = view.getWidth()/(2*DOT_SIZE)+2;
            players1[1].y = view.getHeight()/DOT_SIZE-2;
            setPlayers(players1);
        }
        }
        public void checkBorderLevelOne() {
            if (levelOne.getBorderX()<=players[0].x*DOT_SIZE){
                Player[] players1 = getPlayers();
                this.players[1]=players1[1];
                players1[0].x = view.getWidth()/(2*DOT_SIZE)-2;
                players1[0].y = view.getHeight()/DOT_SIZE-2;
                setPlayers(players1);
            }
            if ((players[1].x*DOT_SIZE)<=levelOne.getBorderX()){
                Player[] players1 = getPlayers();
                this.players[0]=players1[0];
                players1[1].x = view.getWidth()/(2*DOT_SIZE)+2;
                players1[1].y = view.getHeight()/DOT_SIZE-2;
                setPlayers(players1);
            }
        }
    public void doStep(Direction dirNowFS, int player) {
        switch (dirNowFS) {
            case LEFT:
                players[player].x -= 1;
                break;
            case RIGHT:
                players[player].x += 1;
                break;
            case UP:
                players[player].y -=1;
                break;
            case DOWN:
                players[player].y += 1;
                break;
        }
        checkBorder();
        checkBorderLevelOne();
        try {
            out.writeInt(players[0].x);
            out.writeInt(players[0].y);
            out.writeInt(players[1].x);
            out.writeInt(players[1].y);
        } catch (IOException e) {
            e.printStackTrace();
        }
        view.repaint();
    }
    public void setPlayers(Player[] players) {
        this.players = players;
    }
}
